import os
import pandas as pd
import matplotlib.pyplot as plt

# Obtener la ruta del directorio actual (donde se encuentra main.py)
current_directory = os.path.dirname(os.path.abspath(__file__))

# Construir la ruta completa a los archivos CSV
airports_csv_path = os.path.join(current_directory, "..", "data", "airports.csv")
sample_csv_path = os.path.join(current_directory, "..", "data", "sample.csv")

# Cargar los datos desde los archivos CSV usando las rutas completas
airports_df = pd.read_csv(airports_csv_path)
flights_df = pd.read_csv(sample_csv_path)


# Exploración inicial de los datos (primeras filas, informacion y resumen de datos)
def print_data(data):
    print("\n", data, "\n")


print_data(airports_df.head())
print_data(flights_df.head())

print_data(airports_df.info())
print_data(flights_df.info())

print_data(flights_df.describe())

# Variables a analizar (cambiar a los valores que se requieran analizar)
values_to_airport_id = ["OriginAirportID", "DestAirportID"]
airport_id_from = values_to_airport_id[0]  # Valores 0 - 1

values_to_delay = ["DepDelay", "ArrDelay"]
delay_value_from = values_to_delay[0]  # Valores 0 - 1

# Ruta para guardar la imagen de la gráfica
graphic_png_path = os.path.join(
    current_directory,
    "..",
    "report",
    "graphic_" + airport_id_from + "_" + delay_value_from + ".png",
)

# Tratar valores nulos si los hay
airports_df.dropna(inplace=True)
flights_df.dropna(inplace=True)

# Eliminar duplicados si los hay
airports_df.drop_duplicates(inplace=True)
flights_df.drop_duplicates(inplace=True)

# Asegurarse de que los tipos de datos sean adecuados
flights_df[values_to_delay[0]] = flights_df[values_to_delay[0]].astype(int)
flights_df[values_to_delay[1]] = flights_df[values_to_delay[1]].astype(int)

# Identificar los aeropuertos con más atrasos
range_airports = 10  # Resultados a mostrar

top_airports = (
    flights_df.groupby(airport_id_from)[delay_value_from]
    .mean()
    .sort_values(ascending=False)
    .head(range_airports)
)

# Crear un diccionario para almacenar los días con mayor retraso por aeropuerto
top_airports_delay_days = {}

# Diccionario para mapear los números de día a nombres
day_names = {
    1: "Monday",
    2: "Tuesday",
    3: "Wednesday",
    4: "Thursday",
    5: "Friday",
    6: "Saturday",
    7: "Sunday",
}

# Itera a través de los aeropuertos principales
for airport_id in top_airports.index:
    # Filtra los datos de vuelos
    airport_data = flights_df[flights_df[airport_id_from] == airport_id]

    # Calcula el promedio de retraso por día de la semana
    airport_data_by_day = airport_data.groupby("DayOfWeek")[delay_value_from].mean()

    # Encuentra el día de la semana con el mayor promedio de retraso y su valor.
    day_with_max_delay = airport_data_by_day.idxmax()
    max_delay_value = airport_data_by_day.max()

    # Obtiene el nombre del día de la semana
    day_name = day_names[day_with_max_delay]

    # Almacena los resultados en un diccionario de datos con el ID del aeropuerto como clave.
    top_airports_delay_days[airport_id] = {
        "DayOfWeek": day_name,
        "MaxDelay": max_delay_value,
    }

# Crear un DataFrame a partir del diccionario
top_airports_delay_days_df = pd.DataFrame.from_dict(
    top_airports_delay_days, orient="index"
).reset_index()

top_airports_delay_days_df.columns = [airport_id_from, "DayOfWeek", "MaxDelay"]

# Obtener el nombre del aeropuerto a partir de airports_df
top_airports_delay_days_df = top_airports_delay_days_df.merge(
    airports_df[["airport_id", "name"]],
    left_on=airport_id_from,
    right_on="airport_id",
    how="left",
)

# Crear la gráfica de barras
plt.figure(figsize=(15, 8))

plt.bar(
    top_airports_delay_days_df["name"]
    + " ("
    + top_airports_delay_days_df[airport_id_from].astype(str)
    + ")",
    top_airports_delay_days_df["MaxDelay"],
    color="skyblue",
    label="Average delay",
)

plt.xlabel("Airport (ID)")
plt.ylabel("Average delay")

plt.title("Average delay and Day with longest delay by airport")
plt.xticks(rotation=45)
plt.legend(loc="upper right")

for i, row in top_airports_delay_days_df.iterrows():
    plt.text(i, row["MaxDelay"], row["DayOfWeek"], ha="center", va="bottom")

plt.tight_layout()

# Guardar y mostrar los gráficos en archivos
plt.savefig(graphic_png_path)
print_data("__________Image of graphic save in folder report__________")

plt.show()

# Presentación de resultados en consola
print_data(
    "__________Report using "
    + airport_id_from
    + " & "
    + delay_value_from
    + "__________"
)

print_data(top_airports_delay_days_df)
