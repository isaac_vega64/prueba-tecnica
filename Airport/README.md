# Análisis de Datos de Vuelos

Este proyecto en Python realiza un análisis de datos de vuelos y crea una gráfica de barras que muestra el promedio de retraso para cada aeropuerto y el día de la semana con el mayor promedio de retraso en el mismo aeropuerto.

## Ejecución del Código

1. Asegúrate de tener Python instalado en tu sistema.
2. Puedes instalar las bibliotecas necesarias usando pip:

    ```
    pip install pandas matplotlib
    ```

    O bien usando

    ```
    pip install -r requirements.txt
    ```

3. Abre una consola en la carpeta raíz del proyecto.
4. Ejecuta el siguiente comando para realizar el análisis de datos y generar la gráfica:

    ```
    python src/main.py
    ```

## Estructura del Proyecto

El proyecto está organizado de la siguiente manera:

-   `src/`: Carpeta principal que contiene el código para realizar el análisis de datos y crear la gráfica de barras.
-   `data/`: Carpeta que contiene los archivos CSV de datos de aeropuertos y vuelos.
-   `report/`: Carpeta donde se guarda la imagen de la gráfica generada.

## Descripción

-   El código carga los datos desde archivos CSV ubicados en la carpeta `data/`.
-   Realiza una limpieza inicial de los datos, eliminando valores nulos y duplicados.
-   Identifica los aeropuertos con más atrasos y el día de la semana con el mayor promedio de retraso para cada aeropuerto.
-   Crea una gráfica de barras que muestra esta información, incluyendo el nombre del aeropuerto y su ID.

## Resultados

Los resultados del análisis se muestran en la consola y se guardan en una imagen en la carpeta `report/`. La gráfica de barras muestra el promedio de retraso para cada aeropuerto y el día de la semana con el mayor retraso.

## Personalización

El código es altamente personalizable y se pueden cambiar los valores de las variables `airport_id_from` y `delay_value_from` para analizar diferentes aspectos de los datos.
