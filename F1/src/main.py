import os
import pandas as pd
from geopy.distance import geodesic

# Obtener la ruta del directorio actual (donde se encuentra main.py)
current_directory = os.path.dirname(os.path.abspath(__file__))

# Construir la ruta completa a los archivos XLSX
locations_xlsx_path = os.path.join(
    current_directory, "..", "data", "Location Data.xlsx"
)
round_oficial_xlsx_path = os.path.join(
    current_directory, "..", "data", "Official F1 Round Order.xlsx"
)

official_distance_path = os.path.join(
    current_directory, "..", "report", "Official distance.xlsx"
)
closest_distance_path = os.path.join(
    current_directory, "..", "report", "Closest_distance.xlsx"
)

# Cargar los datos desde los archivos XLSX usando las rutas completas
locations_df = pd.read_excel(locations_xlsx_path)
round_oficial_df = pd.read_excel(round_oficial_xlsx_path)


# Exploración inicial de los datos (primeras filas, informacion y resumen de datos)
def print_data(data):
    print("\n", data, "\n")


# print_data(locations_df.head())
# print_data(round_oficial_df.head())

# print_data(locations_df.info())
# print_data(round_oficial_df.info())

# print_data(locations_df.describe())
# print_data(round_oficial_df.describe())

# Se unen los dataframes en función del nombre del circuito
merged_data = round_oficial_df.merge(
    locations_df, left_on="Circuit Name", right_on="CircuitName"
)

# Se elimina la columna 'Circuit Name'
merged_data.drop("Circuit Name", axis=1, inplace=True)

# Se configura la columna 'Round NR' como el índice
merged_data.set_index("Round NR", inplace=True)


# Fucnión para calcular distancias
def calculate_distance(location1, location2):
    return geodesic(location1, location2).kilometers


# Variable glotal de total de distancia
total_distance = 0


# Calcular las distancias de un circuito a otro
def calculate_distance_circuits(data):
    tmpArray = []
    global total_distance

    for i in range(1, len(data)):
        loc1 = (data["Location LAT"][i], data["Location LON"][i])

        loc2 = (
            data["Location LAT"][i + 1],
            data["Location LON"][i + 1],
        )

        distance = calculate_distance(loc1, loc2)
        tmpArray.append(distance)
        total_distance += distance

        if i + 1 == len(data):
            lastDistance = 0
            tmpArray.append(lastDistance)

    return tmpArray


# Calcular las distancias entre circuitos sin modificar el orden de los datos
merged_data["Distance to next"] = calculate_distance_circuits(merged_data)

# imprimir y guardar los resultados
print_data("__________Data distance official__________")
print_data(merged_data)
print_data(f"__________Distancia total: {total_distance} km__________")
merged_data.to_excel(official_distance_path)


# Calcular las distancias de un circuito a otro partiendo de un circuito en especifico
# Inicializar un diccionario para almacenar el orden de los circuitos más cercanos
circuit_order = {}

# Inicializar una lista con los nombres de todos los circuitos
circuit_names = locations_df["CircuitName"].tolist()

# Elegir el primer circuito de inicio (se puedo modificar)
current_circuit = "Bahrain International Circuit"


def get_location(name):
    return locations_df.loc[
        locations_df["CircuitName"] == name,
        ["Location LAT", "Location LON"],
    ].values[0]


# Mientras haya circuitos sin visitar
while circuit_names:
    # Remover el circuito actual de la lista de circuitos disponibles
    circuit_names.remove(current_circuit)

    # Calcular distancias desde el circuito actual a todos los demás circuitos
    distances = {}

    current_location = get_location(current_circuit)

    for circuit_name in circuit_names:
        location = get_location(circuit_name)

        distance = calculate_distance(current_location, location)

        distances[circuit_name] = distance

    # Verificar si quedan circuitos por visitar
    if circuit_names:
        # Encontrar el circuito más cercano
        closest_circuit = min(distances, key=distances.get)

        # Almacenar el circuito más cercano en el diccionario de orden
        circuit_order[current_circuit] = closest_circuit

        # Establecer el circuito más cercano como el nuevo circuito actual
        current_circuit = closest_circuit
    else:
        # Si no quedan circuitos, asignar el mismo circuito y salir del bucle
        circuit_order[current_circuit] = current_circuit

        break

# Extraer los nombres con el nuevo orden
new_names_order = list(circuit_order.keys())

# Crear un nuevo DataFrame ordenado en base a new_names_order
sorted_merged_data = merged_data[merged_data["CircuitName"].isin(new_names_order)]
sorted_merged_data = (
    sorted_merged_data.set_index("CircuitName").loc[new_names_order].reset_index()
)

# Reiniciar el índice comenzando en 1 y renombrarlo como "Round NR"
sorted_merged_data.index = sorted_merged_data.index + 1
sorted_merged_data.index.name = "Round NR"

# Resetear la variable global
total_distance = 0

# Calcular las distancias entre circuitos
sorted_merged_data["Distance to next"] = calculate_distance_circuits(sorted_merged_data)

# imprimir y guardar los resultados
print_data("__________Data distance closest__________")
print_data(sorted_merged_data)
print_data(f"__________Distancia total: {total_distance} km__________")
sorted_merged_data.to_excel(closest_distance_path)
