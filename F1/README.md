# Análisis de Datos F1

Este proyecto de Python realiza un análisis de datos relacionados con las carreras de Fórmula 1, utilizando archivos XLSX para cargar y procesar la información.

## Requisitos y ejecución

1. Asegúrate de tener Python instalado en tu sistema.
2. Puedes instalar las bibliotecas necesarias usando pip:

    ```
    pip install pandas geopy
    ```

    O bien usando

    ```
    pip install -r requirements.txt
    ```

3. Abre una consola en la carpeta raíz del proyecto.
4. Ejecuta el siguiente comando para realizar el análisis de datos y generar los archivos con los resultados:
    ```
    python src/main.py
    ```

## Estructura del Proyecto

El proyecto está organizado de la siguiente manera:

-   `src/`: Carpeta principal que contiene el código para realizar el análisis de datos y crear la gráfica de barras.
-   `data/`: Carpeta que contiene los archivos CSV de datos de aeropuertos y vuelos.
-   `report/`: Carpeta donde se guardan los archivos generados.

## Descripción

El programa realiza las siguientes tareas:

-   Carga los datos de ubicación y orden establecido como "oficial" de carreras desde archivos XLSX.
-   Calcula las distancias entre circuitos en base a las coordenadas geográficas.
-   Crea dos informes en archivos XLSX: uno basado en el orden oficial de las carreras y otro en el orden de circuitos más cercanos desde un circuito de inicio específico.

## Resultados

Los resultados de los análisis se guardan en los siguientes archivos XLSX:

-   **Official distance.xlsx**: Contiene información sobre distancias en el orden oficial de las carreras.
-   **Closest_distance.xlsx**: Contiene información sobre distancias en el orden de circuitos más cercanos.

## Personalización

-   Puedes personalizar la ubicación de los archivos de datos y resultados en el archivo `main.py` modificando las rutas.

-   Puedes personalizar el circuito de inicio para obtener otro tipo de resultados
